package com.jsite.modules.sys.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jsite.common.persistence.TreeEntity;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 区域Entity
 ** @author liuruijun
 * @version 2018-11-15
 */
public class Area extends TreeEntity<Area> {

	private static final long serialVersionUID = 1L;
	private String code; 	// 区域编码
	private String type; 	// 区域类型（1：国家；2：省份、直辖市；3：地市；4：区县）

	public Area(){
		super();
	}

	public Area(String id){
		super(id);
	}

	@JsonIgnore
	@Override
	public boolean getIsRoot() {
		return ROOT_ID.equals(parent.getId());
	}

	@Length(min=1, max=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Length(min=0, max=100)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return name;
	}

	public static void sortList(List<Area> list, List<Area> sourcelist, String parentId, boolean cascade){
		for (int i=0; i<sourcelist.size(); i++){
			Area e = sourcelist.get(i);
			if (e.getParent()!=null && e.getParent().getId().equals(parentId)){
				list.add(e);
				if (cascade){
					// 判断是否还有子节点, 有则继续获取子节点
					for (int j=0; j<sourcelist.size(); j++){
						Area child = sourcelist.get(j);
						if (child.getParent()!=null && child.getParent().getId().equals(e.getId())){
							sortList(list, sourcelist, e.getId(), true);
							break;
						}
					}
				}
			}
		}
	}
}